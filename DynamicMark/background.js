"use strict";

let bookmarkTabs = {};
let dynamicBookmarks = {};

let icon_active = {
	"16": "icons/active16.png",
	"24": "icons/active24.png",
	"32": "icons/active32.png",
	};

let icon_inactive = {
	"16": "icons/inactive16.png",
	"24": "icons/inactive24.png",
	"32": "icons/inactive32.png",
	};


read_persistence();


function log_func(name, params = []) {
	console.groupCollapsed("func '" + name + "' called");
	for (let param of params) {
		console.log("%s: %o", param[0], param[1]);
	}
	console.log("bookmarkTabs: %o", bookmarkTabs);
	console.log("dynamicBookmarks: %o", dynamicBookmarks);
	console.groupEnd();
}


function init() {
	log_func('init');

	chrome.tabs.query({}, tabs => {
			//console.log('  tabs: %o', tabs);
			for (let tab of tabs) {
				try {
					//console.log('  tab: %o', tab);
					chrome.bookmarks.search({url: tab.url}, bookmarks => {
						//console.log('    bookmarks: %o', bookmarks);
						if (bookmarks.length > 0) {
							chrome.pageAction.show(tab.id);
							console.log("tab %o is showing bookmark %o. showing page action", tab, bookmarks);
							// icon will be set later in this func
						}
					});
				} catch (error) {
					console.log(error);
				}
			}
		});

	for (let dynamicBookmarkId in dynamicBookmarks) {
		console.log("dynamic bookmark id: %s", dynamicBookmarkId);
		chrome.bookmarks.get(dynamicBookmarkId, bookmarks => {
			if (bookmarks.length == 0) {
				console.log("bookmark with id '" + dynamicBookmarkId + "' doesn't exist (promise fulfilled with empty array");
				delete dynamicBookmarks[invalidBookmarkId];
				write_persistence();
			}
			if (bookmarks.length > 1) {
				console.log("multiple bookmarks with id: '" + dynamicBookmarkId + "':");
				for (let bookmark of bookmarks) { console.log(bookmark); }
			}
			for (let bookmark of bookmarks) {
				console.log("  bookmark: %o", bookmark);
				//chrome.tabs.query({url: bookmark.url},
				chrome.tabs.query({}, tabs => {
					if (tabs.length == 0) {
						//console.log("    no tabs");
					}
					for (let tab of tabs) {
						if (tab.url == bookmark.url) {
							console.log("    tab: %o", tab);
							bookmarkTabs[tab.id] = {bookmarkId: bookmark.id, tabId: tab.id, dynamic: true};
							restoreScroll(tab.id, dynamicBookmarks[dynamicBookmarkId]);
							chrome.pageAction.setIcon({path: icon_active, tabId: tab.id});
							chrome.pageAction.show(tab.id);
							chrome.tabs.executeScript(tab.id, {file: 'content.js'});
							console.log(
								'tab %o shows bookmark %o with dynamicBookmarkId %o.' +
								'showing page action and active icon',
								tab, bookmark, dynamicBookmarkId);
						}
					}
				});
			}
		});
	}
}


function read_persistence() {
	log_func('read_persistence');
	let default_values = {dynamicBookmarks: {}};
	chrome.storage.local.get(default_values, items => {
		log_func('storage.local.get callback', [['items', items]]);
		dynamicBookmarks = items.dynamicBookmarks;
		init();
	});
}


function write_persistence() {
	log_func('write_persistence');
	chrome.storage.local.set({dynamicBookmarks: dynamicBookmarks});

	console.log('dynamicBookmarks from storage:');
	chrome.storage.local.get('dynamicBookmarks', items => console.log(items.dynamicBookmarks));
}


chrome.tabs.onCreated.addListener(tab => {
	log_func('chrome.tabs.onCreated Listener', [['tab', tab]]);
	if ('url' in tab && tab.url && tab.url.length > 0) {
		no_tab_data_found(tab.id, tab.url);
	} else {
		console.log('tab created without url');
	}
});


chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
	log_func('chrome.tabs.onRemoved Listener', [['tabId', tabId], ['removeInfo', removeInfo]]);
	console.log("bookmarkTabs: %o", bookmarkTabs);
	delete bookmarkTabs[tabId];
	write_persistence();
});


chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
	if (!changeInfo.url) return;
	log_func('chrome.tabs.onUpdated Listener', [['tabId', tabId], ['changeInfo', changeInfo]]);
	if (changeInfo.url) {
		console.log("bookmarkTabs: %o", bookmarkTabs);
		let tabData = bookmarkTabs[tabId];
		if (tabData) {
			chrome.pageAction.setIcon({path: icon_active, tabId: tabId});
			chrome.pageAction.show(tabId);
			console.log("updating dynamic bookmark");
			chrome.bookmarks.update(tabData.bookmarkId, {url: changeInfo.url});
			chrome.tabs.executeScript(tabId, {file: 'content.js'});
		} else {
			no_tab_data_found(tabId, changeInfo.url);
		}
	}
});


chrome.tabs.onActivated.addListener(activeInfo => {
	//log_func('tabs.onActivated Listener', [['activeInfo', activeInfo]]);

	/*
	let tabData = bookmarkTabs[activeInfo.tabId];
	if (!tabData) {
		console.log('tab data not found');
		chrome.pageAction.hide(activeInfo.tabId);
	}
	*/
});


function restoreScroll(tabId, dynamicBookmark) {
	if (dynamicBookmark.scroll) {
		let x = dynamicBookmark.scroll.x;
		let y = dynamicBookmark.scroll.y;
		console.log("restoring scroll position for tabId %o, dynamic bookmark %o to (%o, %o)", tabId, dynamicBookmark, x, y);
		chrome.tabs.executeScript(tabId,
				{code: 'window.scroll(' + x + ',' + y + ');'});
	}
}


function no_tab_data_found(tabId, url) {
	chrome.bookmarks.search({url: url}, bookmarks => {
		if (bookmarks.length > 0) {
			// current tab is bookmarked
			let dynamic = false;
			for (let bookmark of bookmarks) {
				if (bookmark.id in dynamicBookmarks) {
					// current tab is dynamic bookmark
					restoreScroll(tabId, dynamicBookmarks[bookmark.id]);
					console.log("bookmark %o is in dynamicBookmarks %o", bookmark, dynamicBookmarks[bookmark.id]);
					console.log("bookmarkTabs: %o", bookmarkTabs);
					bookmarkTabs[tabId] = {bookmarkId: bookmark.id, tabId: tabId, dynamic: true};
					chrome.tabs.executeScript(tabId, {file: 'content.js'});
					dynamic = true
					break;
				}
			}
			let icon = (dynamic ? icon_active : icon_inactive);
			chrome.pageAction.setIcon({path: icon, tabId: tabId});
			chrome.pageAction.show(tabId);
		} else {
			// current tab is not bookmarked
			chrome.pageAction.hide(tabId);
			chrome.pageAction.setIcon({path: icon_inactive, tabId: tabId});
		}
	});
}


/*
chrome.webNavigation.onCommitted.addListener(details => {
	if (details.frameId !== 0 || details.parentFrameId !== -1) return;
	chrome.tabs.get(details.tabId, tab => {
		console.log('(%o) (%o) %o -> %o',
			details.transitionType, details.transitionQualifiers, tab.url, details.url);
		let url = new URL(details.url);
		console.log("url: %o", url);
	});
});
*/


chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	log_func('runtime.onMessage Listener', [['message', message], ['sender', sender], ['sendResponse', sendResponse]]);

	if ('scroll' in message) {
		console.log('x=' + message.scroll.x + ', y=' + message.scroll.y);

		let tabData = bookmarkTabs[sender.tab.id];
		let dynamicBookmark = dynamicBookmarks[tabData.bookmarkId];
		dynamicBookmark.scroll = {x: message.scroll.x, y: message.scroll.y};
	} else {
		console.log("unknown message received: %o from %o", message, sender);
	}
});


chrome.pageAction.onClicked.addListener(tab => {
	log_func('pageAction.onClicked Listener', [['tab', tab]]);

	// address bar icon clicked (address bar icon only visible if tab shows a bookmarked page)
	// toggle dynamic bookmark for this bookmark
	
	let tabData = bookmarkTabs[tab.id];
	if (!tabData) {
		console.log("bookmarkTab for tab %o doesn't exist", tab);
		console.log("bookmarkTabs: %o", bookmarkTabs);
		// toggle on
		chrome.bookmarks.search({url: tab.url}, bookmarks => {
			console.log("bookmarks for tab: %o", bookmarks);
			if (bookmarks.length == 0) {
				console.log("pageAction was visible for tab %o but no bookmark found", tab);
			}
			for (let bookmark of bookmarks) {
				bookmarkTabs[tab.id] = {bookmarkId: bookmark.id, tabId: tab.id, dynamic: true};
				dynamicBookmarks[bookmark.id] = {dynamic:true};
				write_persistence();
				chrome.pageAction.setIcon({path: icon_active, tabId: tab.id});
				chrome.tabs.executeScript(tab.id, {file: 'content.js'});
				break;
			}
		});
	} else {
		console.log("bookmarkTab for tab with id %s found", tab.id);
		delete bookmarkTabs[tab.id];
		delete dynamicBookmarks[tabData.bookmarkId];
		write_persistence();
		chrome.pageAction.setIcon({path: icon_inactive, tabId: tab.id});
		chrome.tabs.sendMessage(tab.id, 'removeScrollListener');
	}
});


chrome.bookmarks.onCreated.addListener((id, bookmark) => {
	log_func('bookmarks.onCreated Listener', [['id', id], ['bookmark', bookmark]]);

	/*
	console.log('bookmark (%o, %o) removed. deleting dynamic bookmark %o',
		id, bookmark.url, dynamicBookmarks[id]);
	delete dynamicBookmarks[id];
	write_persistence();
	*/

	console.log('bookmark (%o, %o) created', id, bookmark.url);

	//chrome.tabs.query({url: bookmark.url},
	chrome.tabs.query({}, tabs => {
		log_func('tabs.query callback', [['tabs', tabs]]);
		for (let tab of tabs) {
			if (tab.url == bookmark.url) {
				console.log("tab (%o, %o) show pageAction", tab.id, tab.url);
				//bookmarkTabs[tab.id] = {bookmarkId:id, tabId: tab.id, dynamic: false};
				//delete bookmarkTabs[tab.id];
				//chrome.pageAction.setIcon({path: icon_inactive, tabId: tab.id});
				chrome.pageAction.show(tab.id);
			}
		}
	});
});


chrome.bookmarks.onRemoved.addListener((id, removeInfo) => {
	log_func('bookmarks.onRemoved Listener', [['id', id], ['removeInfo', removeInfo]]);

	console.log('bookmark (%o, %o) removed. deleting dynamic bookmark %o',
		id, removeInfo.node.url, dynamicBookmarks[id]);
	delete dynamicBookmarks[id];
	write_persistence();

	console.log('query tabs');
	//chrome.tabs.query({url: removeInfo.node.url},
	chrome.tabs.query({}, tabs => {
		log_func('tabs.query callback', [['tabs', tabs]]);
		for (let tab of tabs) {
			if (tab.url == removeInfo.node.url) {
				console.log('bookmark for tab deleted %o',
					{tab: tab, bookmark: removeInfo.node, bookmarkTab: bookmarkTabs[tab.id]});
				show_pageAction_if_bookmarked(tab);
			}
		}
	});
});


chrome.bookmarks.onChanged.addListener((id, changeInfo) => {
	log_func('bookmarks.onChanged Listener', [['id', id], ['changeInfo', changeInfo]]);
	
	chrome.tabs.query({}, tabs => {
		for (let tab of tabs) {
			try {
				show_pageAction_if_bookmarked(tab);
			} catch (error) {
				console.log(error);
			}
		}
	});
});


function show_active_icon_if_dynamic(tab, bookmarks) {
	let dynamic = false;
	for (let bookmark of bookmarks) {
		if (bookmark.id in dynamicBookmarks) {
			dynamic = true;
		}
	}
	let icon = dynamic ? icon_active : icon_inactive;
	console.log('showing icon %o for pageAction (%o)',
			tab, {tab: tab, bookmarks: bookmarks});
	chrome.pageAction.setIcon({path: icon, tabId: tab.id});
}


function show_pageAction_if_bookmarked(tab) {
	chrome.bookmarks.search({url: tab.url}, bookmarks => {
		if (bookmarks.length > 0) {
			console.log('showing pageAction for tab (%o, %o), bookmarks %o',
					tab.id, tab.url, bookmarks);
			show_active_icon_if_dynamic(tab, bookmarks);
			chrome.pageAction.show(tab.id);
			//console.log("bookmarkTabs: %o", bookmarkTabs);
			//delete bookmarkTabs[tab.id];
			//console.log("bookmarkTabs: %o", bookmarkTabs);
		} else {
			/*console.log('hiding pageAction for tab (%o, %o), no bookmarks found',
					tab.id, tab.url);*/
			chrome.pageAction.hide(tab.id);
			chrome.pageAction.setIcon({path: icon_inactive, tabId: tab.id});
		}
	});
}

