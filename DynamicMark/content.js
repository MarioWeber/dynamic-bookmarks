chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	console.log('chrome.runtime.onMessage listener: %o',
		{message, sender, sendResponse});
	if (message == 'removeScrollListener') {
		window.removeEventListener('scroll', eventListener);
	}
});

function eventListener(event) {
	chrome.runtime.sendMessage(
		{scroll: {x: window.scrollX, y: window.scrollY}});
}

window.addEventListener('scroll', eventListener);

